import Card from '../UI/Card';
import classes from './Cart.module.css';
import CartItem from './CartItem';

import { useSelector } from 'react-redux';      // getting data from the store

const Cart = (props) => {
    const cartItems = useSelector((state) => state.cart.items);  // // getting data from the store cartitems i.e items
    return(
    <Card className={classes.cart}>
        <h2>your Shopping cart</h2>
        <ul>
            {cartItems.map((item) => (                  // passing the data to cartItem components..

                <CartItem 
                key={item.id}
                item={{ 
                    id :item.id,
                    title: item.name, 
                    quantity: item.quantity,
                    total: item.totalPrice,
                    price: item.price }}
             />
            ))}
            
        </ul>
    </Card>
    );

}
export default Cart;