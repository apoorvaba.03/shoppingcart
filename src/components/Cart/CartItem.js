import classes from './CartItem.module.css';
import { useDispatch } from 'react-redux';
import { cartActions } from '../../store/cart-slice';

const CartItem = (props) => {
  const { title, quantity, total, price,id } = props.item;  // getting data from cart.js
  const dispatch=useDispatch();     //dispatching the action
  const addItemHandler =() =>{            // by clicking button 

    console.log("inside addtocart+");
    dispatch(cartActions.addItemToCart({    // dispatch the action to addItemcart in reduc cardslice
      id,
      price,
      title,
    }
    ));
  }

  const removeItemHandler = () => {
    dispatch(cartActions.removeItemToCart(id))   // for removing item from cart calling thee method in cardslice.
  }

  return (
    <li className={classes.item}>
      <header>
        <h3>{title}</h3>
        <div className={classes.price}>
          ${total.toFixed(2)}{' '}
          <span className={classes.itemprice}>(${price.toFixed(2)}/item)</span>
        </div>
      </header>
      <div className={classes.details}>
        <div className={classes.quantity}>
          x <span>{quantity}</span>
        </div>
        <div className={classes.actions}>
          <button onClick={removeItemHandler}>-</button>
          <button onClick={addItemHandler}>+</button>
        </div>
      </div>
    </li>
  );
};

export default CartItem;