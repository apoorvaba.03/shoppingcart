import ProductItem from './ProductItem';
import classes from './Products.module.css';


const DUMMY_PRODUCTS = [
  {
    id:'p1',
    price:10,
    title:'My First Book',
    description:'happiness'
  },
  {
    id:'p2',
    price:20,
    title:'My second book',
    description:'my Life'
  },
]

const Products = (props) => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        { DUMMY_PRODUCTS.map((product) =>(            // passing data to productItem.
          <ProductItem
          key = {product.id}
          id ={product.id}
          title={product.title}
          price={product.price}
          description={product.description}
        />
        ) 

        )}
        
      </ul>
    </section>
  );
};

export default Products;