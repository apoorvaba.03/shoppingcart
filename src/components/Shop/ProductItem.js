import Card from '../UI/Card';
import classes from './ProductItem.module.css';
import { useDispatch } from 'react-redux';
import { cartActions } from '../../store/cart-slice';

const ProductItem = (props) => {
  const { title, price,id, description } = props; // getting data from product.js
  const dispatch = useDispatch();
  const addToCartHandler = () => {                // adding the data to cart via store i.e cartSlice
    dispatch(cartActions.addItemToCart(
      {
        id,
        price,
        title,
      }
    ));
    
  }

  return (
    <li className={classes.item}>
      <Card>
        <header>
          <h3>{title}</h3>
          <div className={classes.price}>${price.toFixed(2)}</div>
        </header>
        <p>{description}</p>
        <div className={classes.actions}>
          <button onClick={addToCartHandler}>Add to Cart</button>
        </div>
      </Card>
    </li>
  );
};

export default ProductItem;