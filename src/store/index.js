import { configureStore } from "@reduxjs/toolkit";   // central data store
import uiSlice from './ui-slice';
import cartSlice from "./cart-slice";

const store=configureStore({
    reducer : { ui : uiSlice.reducer , cart:cartSlice.reducer }     // for multiple slicess.
})
export default store;