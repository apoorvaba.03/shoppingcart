import { createSlice } from '@reduxjs/toolkit';     // creating slice

const  uiSlice= createSlice({
    name:'ui',                                       // name, initialState,reducers are property of createSlice 
    initialState:{cartIsVisible: false , notification:null},
    reducers : {
        toggle(state){                                        // method to perform for outside event
            state.cartIsVisible = !state.cartIsVisible;
        },
        setNotification(state,action){
            state.notification = {
                status : action.payload.status,
                title:action.payload.title,
                message:action.payload.message,
            };
        },
    },
})

export const uiActions = uiSlice.actions;                    // to avid typo error of identifiers sending action type
export default uiSlice;

