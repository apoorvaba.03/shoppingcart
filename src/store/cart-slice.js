import { createSlice } from "@reduxjs/toolkit";

const cartSlice=createSlice({
    name:'cart',
    initialState:{                          // initial value items and quantity
        items:[],
        totalQuantity:0,
    },
    reducers:{
        addItemToCart(state,action){                    // reducer function
            const newItem = action.payload;                 // getting item
            const existingItem = state.items.find(item => item.id === newItem.id)   // checking the item
            state.totalQuantity++;                      // increaseing the quantity
            if(!existingItem){                                          
                state.items.push({                          // pushing the item to array
                    id : newItem.id,
                    price:newItem.price,
                    quantity:1,
                    totalPrice:newItem.price,
                    name:newItem.title,
                });

            }else{
                existingItem.quantity++;
                existingItem.totalPrice = existingItem.totalPrice+newItem.price;        // adding price
            }

        },
        removeItemToCart(state,action){
            const id = action.payload;                          // getting id 
            state.totalQuantity--;
            const existingItem = state.items.find(item => item.id === id);  // comparing the item
            if(existingItem.quantity === 1)
            {
                state.items = state.items.filter(item => item.id !== id);  // removing from item array.
            }
            else{
                existingItem.quantity--;
            }
        }
    },
})

export const cartActions = cartSlice.actions;
export default cartSlice;