import Cart from './components/Cart/Cart';
import Layout from './components/Layout/Layout';
import Products from './components/Shop/Products';
import './App.css';
import { useEffect } from 'react';

import { useSelector } from 'react-redux'; 
import { useDispatch } from 'react-redux';     // reading props from store

import { uiActions} from './store/ui-slice';
import Notification from './components/UI/Notification';
import { Fragment} from 'react';

let intial = true;


function App() {

  const dispatch = useDispatch();
  const showCart =useSelector(state => state.ui.cartIsVisible)    // existing state from store ui-slice to render conditionally
  const cart = useSelector(state => state.cart); 
  const notification = useSelector(state => state.ui.notification);

  useEffect(()=>{
    const sendCartData = async () => {
      dispatch(uiActions.setNotification(
        {
          status:'pending',
          title:'sending',
          message:'sending cart data'
        }
      ));


     const response = await fetch('https://react-http-4902c-default-rtdb.firebaseio.com/cart.json',{
      method:'PUT',
      body:JSON.stringify(cart),

    }
  );
    if(!response.ok){
      throw new Error('sending cart data failed');
      
    }
   // const responseData = await response.json();
   dispatch(uiActions.setNotification(
    {
      status:'success',
      title:'Success!',
      message:'sent cart data successfully'
    }
  ));
  
  
  };

  if(intial) {
    intial = false;
    return;
  }
  sendCartData().catch((error) => {
    dispatch(uiActions.setNotification(
      {
        status:'error',
        title:'Error!',
        message:'Sending cart data failed!'
      }
    ));
  })

  },[cart,dispatch]);

  return (
    <Fragment>
     { notification && <Notification
     status = { notification.status}
     title = { notification.title}
     message = { notification.message}
     />}
   <Layout>
    {showCart && <Cart />}                                 
     <Products />
   </Layout>
   </Fragment>
  );
}

export default App;
